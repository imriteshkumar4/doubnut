const { v4: uuidv4 } = require('uuid');
let Service = require('../services/services.userService');
let session={};
let sessionToClear=[];


let controller = {
    clearTimeIntervals: async function(sessionToclears){
        if (sessionToclears.length >0){
            let values = [];
            for (var i=0;i<sessionToclears.length;i++) {
                values.push(sessionToclears[i]);
                clearTimeout(session[sessionToclears[sessionToclears[i]]]);
                delete session[sessionToclears[i]];
            }
            sessionToClear = values.filter(function(obj) { return sessionToclears.indexOf(obj) === -1; });
        }

    },
    getQueryResponse: async function(req, res) {

        let data = req.body; //Which accepts some data in JSON format.
        let conversationId = uuidv4();
        await controller.clearTimeIntervals(sessionToClear);
        let response = await Service.convertPdf(data);
        if (session.hasOwnProperty(conversationId)){
            clearTimeout(session[conversationId]["timeCalculate"]);
            session[conversationId]={
                queryTime: new Date(), // Last Query Time
                response : session[conversationId].response,
                timeCalculate:setTimeout(async function () {
                    res.send(response);                             // Send response after 5 mins of inactivity
                    sessionToClear.push(conversationId);
                }, 300000)
            }
        }
        else {
            session[conversationId] = {
                queryTime: new Date(),
                conversationId: conversationId,
                response:response

            }
        }

    },
}
module.exports = controller;