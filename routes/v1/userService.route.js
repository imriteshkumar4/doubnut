const express = require('express');
const router = express.Router();
const controller = require("../../controllers/controller.userService");
router.route('/user/query')
    .post(controller.getQueryResponse);
module.exports = router;