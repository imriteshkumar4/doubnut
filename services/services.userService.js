var express = require("express");
var pdf = require("html-pdf");
const fs = require("fs");
let services = {
    convertPdf: async function(data){
        const stream = await new Promise((resolve, reject) => {
            pdf.create(data).toStream((err, stream) => {
                if (err) {
                    reject(reject);
                    return;
                }
                resolve(stream);
            });
        });
        const fileName = `${+new Date()}.pdf`;
        const pdfPath = `${__dirname}/../files/${fileName}`;
        let doc = stream.pipe(fs.createWriteStream(pdfPath));
        return doc
    }

}
module.exports = services;